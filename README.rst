PropScraper
===========

PropScraper is a web scraper. It allows you to extract some information from real state websites.

License
-------

PropScraper is offered under the `GNU AGPLv3
<https://gnu.org/licenses/agpl.html>`_.

